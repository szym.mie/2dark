export default class Menu {
    constructor(elem) {
        this.menu = elem;
        this.ms = this.menu.style;

        this.options = [];
    }

    add_option(name, keystroke, action) {
        this.options.push({
            name: name,
            keystroke: keystroke,
            action: action
        });

        const entry = document.createElement("div");
        entry.className = "menu-entry";

        const p_name = document.createElement("p");
        const p_keystroke = document.createElement("p");

        p_name.innerText = name;
        p_keystroke.innerText = keystroke;

        entry.appendChild(p_name);
        entry.appendChild(p_keystroke);

        entry.onclick = ev => {
            action(ev);
            this.hide();
        }

        this.menu.appendChild(entry);
        this.menu.onmouseleave = ev => {
            this.hide();
        }
    }

    show(ev) {
        const px = px => parseInt(px) + "px";
        
        this.menu.className = "menu";
        this.ms.top = px(ev.pageY - 2);
        this.ms.left = px(ev.pageX - 2);
    }

    hide() {
        this.menu.className = "menu-hidden";
    }
}