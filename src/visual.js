export default class Visual {
    constructor(canvas_elem, sprites, board, pieces) {
        this.sprites = sprites;

        this.board = board;
        this.pieces = pieces;

        this.canvas = canvas_elem;
        this.ctx = this.canvas.getContext("2d");

        this.ctx.fillStyle = "#000000";
        this.ctx.strokeStyle = "#ffffff";
    }

    render() {
        // clear rect.
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        
        // draw everything.
        this.pieces.draw(this);
        this.board.draw(this);

        requestAnimationFrame(this.render.bind(this));
    }
}