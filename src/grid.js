export default class Grid {
    constructor(rows, columns, offset, scale) {
        this.rows = rows;
        this.columns = columns;
        this.offset = offset;
        this.scale = scale;
    }

    get_point_bounds(x, y) {
        return [
            Math.floor((y - this.offset[0]) / this.scale[1]),
            Math.floor((x - this.offset[1]) / this.scale[0])
        ];
    }

    get_area_bounds(x0, y0, x1, y1) {
        // start coord.
        const xs = Math.min(x0, x1) - this.offset[0];
        const ys = Math.min(y0, y1) - this.offset[1];
        // width and height.
        const dx = Math.abs(x0 - x1), dy = Math.abs(y0 - y1);

        const xe = xs + dx, ye = ys + dy;
        
        return [
            Math.floor(xs / this.scale[0]),
            Math.floor(ys / this.scale[1]),
            Math.floor(xe / this.scale[0]),
            Math.floor(ye / this.scale[1])
        ];
    }

    draw_lines(vis) {
        const [w, h] = this.scale;

        const pw = this.columns * w;
        const ph = this.rows * h;

        for (let x = 0; x <= this.columns; x++) {
            const xc = x * w;
            vis.ctx.moveTo(xc, 0);
            vis.ctx.lineTo(xc, ph);
        }

        for (let y = 0; y <= this.rows; y++) {
            const yc = y * h;
            vis.ctx.moveTo(0, yc);
            vis.ctx.lineTo(pw, yc);
        }

        vis.ctx.stroke();
    }

    _draw_start(vis) {
        vis.ctx.save();
        vis.ctx.translate(this.offset[0], this.offset[1]);
    }

    _draw_end(vis) {
        vis.ctx.restore();
    }
}