const marker = document.getElementById("marker");
const ms = marker.style;

export default class Marker {
    constructor(elem) {
        this.marker = elem;
        this.ms = this.marker.style;

        this.enabled = false;
        this.origin = [0, 0];
        this.last = [0, 0];
    }

    select_start(ev) {
        this.select_clear();
        this.enabled = true;
        this.origin = [ev.pageX, ev.pageY];
        this.last = [...this.origin];
        this.marker.className = "free";
    }

    select_end(on_end) {
        this.marker.className = "free-hidden";

        this.enabled = false;
        const [x0, y0] = this.origin;
        const [x1, y1] = this.last;

        on_end(x0, y0, x1, y1);
    }

    select_update(ev) {
        const px = px => parseInt(px) + "px";

        if (this.enabled) {
            const mx = ev.pageX, my = ev.pageY;

            this.last = [ev.pageX, ev.pageY];

            const dx = window.innerWidth, dy = window.innerHeight;

            const [ox, oy] = this.origin;

    
            if (mx > ox) { // to the right of origin
                ms.left = px(ox);
                ms.right = px(dx - mx);
            } else { // to the left of origin
                ms.left = px(mx);
                ms.right = px(dx - ox);
            }
    
            if (my > oy) { // lower than origin
                ms.top = px(oy);
                ms.bottom = px(dy - my);
            } else { // higher than origin
                ms.top = px(my);
                ms.bottom = px(dy - oy);
            }
        }
    }

    select_clear() {
        ms.top = "unset";
        ms.left = "unset";
        ms.bottom = "unset";
        ms.right = "unset";
    }
}