import Dimensions from "./dimensions.js";
import Grid from "./grid.js";

export default class Board extends Grid {
    constructor(rows, columns) {
        super(
            rows, 
            columns, 
            Dimensions.board, 
            Dimensions.scal);

        this.pieces = [];
        this.selected = [];

        for (let j = rows*columns; j > 0; j--) {
            this.pieces.push(0);
        }
    }

    select_pieces(x0, y0, x1, y1, toggle) {
        const [r0, c0, r1, c1] = this.get_area_bounds(x0, y0, x1, y1);

        const mask = [];
        for (let r = r0; r <= r1; r++) {
            for (let c = c0; c <= c1; c++) {
                const j = r * this.columns + c;
                if (r < 0 || r >= this.rows || c < 0 || c >= this.columns) continue;
                if (toggle && this.selected.includes(j)) mask.push(j);
                if (!this.selected.includes(j)) this.selected.push(j);
            }
        }
        if (toggle) {
            this.selected = this.selected.filter(piece => {
                return !mask.includes(piece);
            });
        }
    }

    fill_pieces(id) {
        for (const j of this.selected) {
            this.pieces[j] = id;
        }
    }

    clear_selection() {
        this.selected = [];
    }

    draw(vis) {
        this._draw_start(vis);

        // board draw.
        const [w, h] = this.scale;

        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.columns; x++) {
                const j = x * this.rows + y;
                const b = this.pieces[j];

                vis.ctx.drawImage(vis.sprites.sprites[b],
                    x * w, y * h,
                    w, h);
            }
        }

        this.draw_lines(vis);
        this.draw_highlights(vis);

        this._draw_end(vis);
    }

    draw_highlights(vis) {
        const [w, h] = this.scale;
        const [bx, by] = Dimensions.border;
        const bw = w - 2 * bx;
        const bh = h - 2 * by;

        vis.ctx.save();
        vis.ctx.strokeStyle = "#ff0000";

        for (const j of this.selected) {
            const r = Math.floor(j / this.columns);
            const c = j % this.columns;

            vis.ctx.strokeRect(r * w + bx, c * h + by, bw, bh);
        }

        vis.ctx.restore();
    }

    read_pieces_file(text) {
        const tokens = text.split(",").map(token => token.trim());
        const len = Math.min(tokens.length, this.columns * this.rows);
        for (let j = 0; j < len; j++) {
            this.pieces[j] = parseInt(tokens[j]);
        }
    }

    write_pieces_file() {
        let text = "";
        for (let j = 0; j < this.pieces.length; j++) {
            text += this.pieces[j].toString()+',';
        }
        return text.slice(0, -1);
    }
}