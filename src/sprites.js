export default class Sprites {
    constructor(spritesheet_url, sprites) {
        this.spritesheet_url = spritesheet_url;
        this.spritesheet_img = new Image();

        this._promises = [];
        this.sprites = [];

        this.loaded = new Promise(resolve => {
            this.spritesheet_img.onload = () => {
                for (const sprite of sprites) {
                    this.add(sprite.x, sprite.y, sprite.w, sprite.h);
                }
                resolve(
                    Promise.all(this._promises)
                    .then(returned => {
                        this.sprites = [...returned];
                    }));

            };
            this.spritesheet_img.src = this.spritesheet_url;
        });
    }

    add(sx, sy, dx, dy) {
        return this._promises.push(
            createImageBitmap(
                this.spritesheet_img, 
                sx, sy, dx, dy)
        ) - 1;
    }


}