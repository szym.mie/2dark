import Sprites from "./sprites.js";

import Dimensions from "./dimensions.js";

import Pieces from "./pieces.js";
import Board from "./board.js";

import UserInput from "./userinput.js";
import Visual from "./visual.js";

const c = document.getElementById("c");

const [w, h] = Dimensions.orig;

const sprites = new Sprites("../gfx/ark.png", [
    {x:   0, y:   0, w: w, h: h},

    {x:   5, y: 216, w: w, h: h},
    {x:  15, y: 216, w: w, h: h},
    {x:  25, y: 216, w: w, h: h},
    {x:  35, y: 216, w: w, h: h},
    {x:  45, y: 216, w: w, h: h},

    {x:   5, y: 221, w: w, h: h},
    {x:  15, y: 221, w: w, h: h},
    {x:  25, y: 221, w: w, h: h},
    {x:  35, y: 221, w: w, h: h},
    {x:  45, y: 221, w: w, h: h},

    {x:   5, y: 226, w: w, h: h},
    {x:  15, y: 226, w: w, h: h},
    {x:  25, y: 226, w: w, h: h},
    {x:  35, y: 226, w: w, h: h},
    {x:  45, y: 226, w: w, h: h},
]);

sprites.loaded.then(() => {
    visual.render();
});

const board = new Board(20, 20);
const pieces = new Pieces([
     1,  2,  3,  4,  5, 
     6,  7,  8,  9, 10, 
    11, 12, 13, 14, 15, 
     0,  0,  0,  0,  0], 5);

const input = new UserInput(
    document.getElementById("marker"),
    document.getElementById("menu"),
    board, 
    pieces);

const visual = new Visual(c, sprites, board, pieces);