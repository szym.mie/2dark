import Marker from "./marker.js";
import Menu from "./menu.js";

export default class UserInput {
    constructor(marker_elem, menu_elem, board, pieces) {
        this.marker = new Marker(marker_elem);
        this.menu = new Menu(menu_elem);
        this.board = board;
        this.pieces = pieces;

        // available keys.
        this.actions = {
            "Delete": this.clear_selection.bind(this),
            "s": this.save_to_file.bind(this),
            "l": this.load_from_file.bind(this)
        };

        // available menu actions.
        this.menu.add_option("select none", "del", this.clear_selection.bind(this));
        this.menu.add_option("load from file", "ctrl-l", this.load_from_file.bind(this));
        this.menu.add_option("save to file", "ctrl-s", this.save_to_file.bind(this));

        // keyboard events.
        document.onkeydown = this.input_keydown.bind(this);

        // mouse events.
        document.onmousedown = this.input_mousedown.bind(this);
        document.onmouseup = this.input_mouseup.bind(this);
        document.onmousemove = this.input_mousemove.bind(this);
    }

    clear_selection() {
        this.board.clear_selection();
    }

    save_to_file(ev) {
        if (ev.ctrlKey || !(ev instanceof KeyboardEvent)) {
            const d = this.board.write_pieces_file();
            const a = document.createElement("a");
            a.href = URL.createObjectURL(new Blob(
                [d], {type: "text/plain"}
            ));
            a.download = "board.txt";
            a.click();
            URL.revokeObjectURL(a.href);
        }
    }

    load_from_file(ev) {
        if (ev.ctrlKey || !(ev instanceof KeyboardEvent)) {
            const i = document.createElement("input");
            i.type = "file";
            i.click();
            i.onchange = iev => {
                const r = new FileReader();
                r.onload = rev => {
                    this.board.read_pieces_file(rev.target.result)
                };
                r.readAsText(iev.target.files[0]);
            };
        }
    }

    input_keydown(ev) {
        if (Object.keys(this.actions).includes(ev.key)) {
            this.actions[ev.key](ev);
            ev.preventDefault();
        }
    }

    input_mousedown(ev) {
        switch (ev.button) {
            case 0:
                this.marker.select_start(ev);
                break;
            case 2:
                this.menu.show(ev);
                break;
            default:
                break;
        }
    }

    input_mouseup(ev) {
        switch (ev.button) {
            case 0:
                this.marker.select_end((x0, y0, x1, y1) => {
                    const pid = this.pieces.pieces_id(x0, y0);
                    if (!(ev.ctrlKey || ev.metaKey) && pid === -1) {
                        this.board.clear_selection();
                    }
                    this.board.select_pieces(x0, y0, x1, y1, (ev.ctrlKey || ev.metaKey));
                    if (pid !== -1) this.board.fill_pieces(pid);
                });
                break;
            default:
                break;
        }
    }

    input_mousemove(ev) {
        this.marker.select_update(ev);
    }
}