import Dimensions from "./dimensions.js";
import Grid from "./grid.js";

export default class Pieces extends Grid {
    constructor(ids, columns) {
        super(
            Math.ceil(ids.length / columns), 
            columns, 
            Dimensions.pieces, 
            Dimensions.scal);

        this.ids = ids;
    }

    pieces_id(x, y) {
        const [r, c] = this.get_point_bounds(x, y);
        const j = r * this.columns + c;
        if (r < 0 || r > this.rows || c < 0 || c > this.columns) return -1;
        else return this.ids[j];
    }

    draw(vis) {
        this._draw_start(vis);

        // pieces draw.
        const [w, h] = this.scale;

        for (let p = 0; p < this.ids.length; p++) {
            const i = this.ids[p];
            const x = p % this.columns;
            const y = Math.floor(p / this.columns);

            vis.ctx.drawImage(
                vis.sprites.sprites[i], 
                x * w, y * h, 
                w, h);
        }

        this.draw_lines(vis);

        this._draw_end(vis);
    }
}