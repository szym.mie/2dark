export default class Dimensions {
    static border = [1, 1];
    static orig = [7, 3];
    static scal = [42, 18];
    static pieces = [50, 50];
    static board = [300, 50];
};